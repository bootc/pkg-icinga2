Source: icinga2
Maintainer: Debian Nagios Maintainer Group <pkg-nagios-devel@lists.alioth.debian.org>
Uploaders: Markus Frosch <lazyfrosch@debian.org>,
           Jan Wagner <waja@cyconet.org>,
           Alexander Wirt <formorer@debian.org>
Section: admin
Priority: optional
Build-Depends: bash-completion,
               bison,
               cmake (>= 2.8.8),
               cmake (>= 3.2.2) | pkg-config,
               debhelper (>= 10.1),
               flex,
               g++ (>= 1.96),
               libboost-dev,
               libboost-program-options-dev,
               libboost-regex-dev,
               libboost-system-dev,
               libboost-test-dev,
               libboost-thread-dev,
               libedit-dev,
       default-libmysqlclient-dev,
               libpq-dev,
               libssl-dev,
               libyajl-dev,
               libwxgtk3.0-dev,
               make (>= 3.81),
               po-debconf
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/nagios-team/pkg-icinga2
Vcs-Git: https://salsa.debian.org/nagios-team/pkg-icinga2.git
Homepage: https://www.icinga.com

Package: icinga2
Architecture: any
Depends: icinga2-bin (= ${binary:Version}),
         icinga2-common (= ${source:Version}),
         ${misc:Depends}
Recommends: icinga2-doc (= ${binary:Version}),
            monitoring-plugins-basic | nagios-plugins-basic,
            libreadline7
Suggests: vim-icinga2
Description: host and network monitoring system
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This is the metapackage to install all Icinga 2 features.

Package: icinga2-common
Architecture: all
Depends: adduser,
         lsb-base,
         lsb-release,
         ${misc:Depends}
Recommends: logrotate
Conflicts: python-icinga2
Provides: python-icinga2
Replaces: python-icinga2
Description: host and network monitoring system - common files
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This package provides configuration and some basic helper scripts.

Package: icinga2-bin
Architecture: any
Depends: icinga2-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: host and network monitoring system - daemon
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This package provides the Icinga 2 daemon.

Package: icinga2-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: host and network monitoring system - documentation
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This package provides the Icinga 2 documentation.

Package: icinga2-classicui
Architecture: all
Depends: apache2-utils,
         dpkg (>= 1.15.7.2),
         icinga-cgi-bin (>= 1.11.5~),
         icinga2-bin (>= 2.2.0~),
         icinga2-common (= ${source:Version}),
         ${misc:Depends}
Recommends: apache2 | httpd
Description: host and network monitoring system - classic UI
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This package provides support for the Icinga Classic User Interface.

Package: icinga2-ido-mysql
Architecture: any
Depends: dbconfig-common,
         icinga2-bin (= ${binary:Version}),
         icinga2-common (= ${source:Version}),
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: default-mysql-client | virtual-mysql-client
Suggests: default-mysql-server | virtual-mysql-server
Pre-Depends: ${misc:Pre-Depends}
Conflicts: icinga2-ido-pgsql
Description: host and network monitoring system - MySQL support
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This package provides the IDO module for the MySQL database.

Package: icinga2-ido-pgsql
Architecture: any
Depends: dbconfig-common,
         icinga2-bin (= ${binary:Version}),
         icinga2-common (= ${source:Version}),
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: postgresql-client
Suggests: postgresql
Pre-Depends: ${misc:Pre-Depends}
Conflicts: icinga2-ido-mysql
Description: host and network monitoring system - PostgreSQL support
 Icinga 2 is a general-purpose monitoring application to fit the needs of
 any size of network. Icinga 1.x was a Nagios fork; this new generation
 has been rewritten from scratch in C++, with multi-threading and cluster
 support.
 .
 Features:
  * all standard features of Icinga and Nagios;
  * much faster and more scalable than Icinga 1 and Nagios;
  * new, more intuitive, template-based configuration format;
  * monitoring services on ICMP (ping) or TCP ports (HTTP, NNTP, POP3,
    SMTP, etc.) by executing checks (see monitoring-plugins*);
  * any small script following the Nagios plugin API can be used as a
    check plugin;
  * notifications about alerts for any custom script (with examples);
  * native support for Livestatus and Graphite.
 .
 This package provides the IDO module for the PostgreSQL database.

Package: vim-icinga2
Architecture: all
Depends: ${misc:Depends}
Recommends: vim-addon-manager
Description: syntax highlighting for Icinga 2 config files in VIM
 The vim-icinga2 package provides filetype detection and syntax
 highlighting for Icinga 2 config files.
 .
 As per the Debian vim policy, installed addons are not activated
 automatically, but the "vim-addon-manager" tool can be used for this
 purpose.
